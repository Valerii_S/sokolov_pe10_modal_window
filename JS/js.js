let myModal = document.getElementById("modal");
let myBtn = document.getElementById("btn");
let cross = document.getElementById("close");


myBtn.onclick = function () {
    myModal.style.display = "block";
};

cross.onclick = function () {
    myModal.style.display = "none";
};

window.onclick = function (event) {
    if(event.target == myModal) {
        myModal.style.display = "none";
    }
};